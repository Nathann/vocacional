class TestResultService
  def initialize(user)
    @user = user
  end

  def perform
    selected = @user.answers.where(content: true)
    result = selected.joins(question:[field_questions: :study_field]).group("study_fields.id").count.max_by{|k,v| v}
    result ? StudyField.find(result.first) : nil
  end

end
