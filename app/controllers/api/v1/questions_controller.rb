class Api::V1::QuestionsController < Api::V1::ApiController
  before_action :set_question, only: [:show]

  # GET /api/v1/questions/1
  def index
    @questions = Question.all.group_by{ |q| q.category.humanize }.to_a
    render json: @questions
  end

  # GET /api/v1/questions/1
  def show
    render json: @question
  end

  private
   # Use callbacks to share common setup or constraints between actions.
   def set_question
     @contact = Question.find(params[:id])
   end
end
