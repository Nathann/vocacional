module Api::V1
  class AnswersController < Api::V1::ApiController

    def retrieve
      answers = current_user.answers
      @answers = if current_user.answers.any?
        answers.includes(:question)
      else
        Question.all.map do |q|
          answers.build(question: q, content: false)
        end
      end
      render json: @answers, methods: [:text, :category, :humanized_category]
    end

    def update
      current_user.update(answer_params)
      result = TestResultService.new(current_user).perform
      current_user.study_field = result
      current_user.save
      if current_user.save && current_user.study_field.present? && current_user.study_field == result
        render json: result, include: {:courses => {:only => :name}}, status: :created
      else
        render json: current_user, status: :unprocessable_entity
      end
    end

    private
    def answer_params
      params.require(:user).permit(answers_attributes: [:id, :content, :question_id])
    end

  end
end
