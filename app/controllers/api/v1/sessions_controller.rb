# frozen_string_literal: true

class Api::V1::SessionsController < Devise::SessionsController
  # protect_from_forgery with: :null_session
  skip_before_action :verify_authenticity_token, only: [:create]
  respond_to :json

  def create
    self.resource = warden.authenticate!({:scope=>:user, :recall=>"api/v1/sessions#new"})
    sign_in("user", resource)
    respond_with_authentication_token(resource)
  end

  protected

  def respond_with_authentication_token(resource)
    render json: {
      success: true,
      auth_token: resource.authentication_token,
      email: resource.email
    }
  end

  # before_action :configure_sign_in_params, only: [:create]

  # GET /resource/sign_in
  # def new
  #   super
  # end

  # POST /resource/sign_in
  # def create
  #   super
  # end

  # DELETE /resource/sign_out
  # def destroy
  #   super
  # end

  # protected

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_sign_in_params
  #   devise_parameter_sanitizer.permit(:sign_in, keys: [:attribute])
  # end
end
