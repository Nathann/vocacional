class User < ApplicationRecord
  include RailsAdminCharts
  acts_as_token_authenticatable
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  has_many :answers
  belongs_to :study_field, optional: true
  validates_associated :answers

  accepts_nested_attributes_for :answers

  def self.graph_data(since = 30.days.ago)
    joins(:study_field).group("study_fields.name").count.to_a
  end

  def self.chart_type
    'pie'
  end
end
