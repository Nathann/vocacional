class Answer < ApplicationRecord
  belongs_to :user
  belongs_to :question
  delegate :text, to: :question
  delegate :category, to: :question

  validates :content, inclusion: { in: [ true, false ] }

  def humanized_category
    category.humanize
  end
end
