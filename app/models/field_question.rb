class FieldQuestion < ApplicationRecord
  belongs_to :question
  belongs_to :study_field

  rails_admin do
    object_label_method do
      :object_label_method
    end
  end

  private
  def object_label_method
    question.text + "/" + study_field.name
  end
end
