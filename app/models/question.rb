class Question < ApplicationRecord
  has_many :answers
  has_many :field_questions
  has_many :study_fields, through: :field_questions

  enum category: {
    ambiente_de_trabalho: 1,
    atividades_de_trabalho: 2,
    objetos_do_trabalho: 3,
    rotina_de_trabalho: 4,
  }

  rails_admin do
    object_label_method do
      :text
    end
  end
end
