Rails.application.routes.draw do

  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  devise_for :users
  namespace :api do
    namespace :v1 do
      resources :questions
      get 'answers/retrieve'
      put 'answers/update'
      patch 'answers/update'

      devise_for :users, defaults: { format: :json }
    end
  end
end
