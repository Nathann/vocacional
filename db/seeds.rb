# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

User.create(email: "nathan@example.org", name: "Nathann", password: "123123123")
User.create(email: "john@example.org", name: "John", password: "123123123")
User.create(email: "user@example.org", name: "user", password: "123123123")

[
  {
    name: "Ciências Exatas",
    courses:  ["Sistemas de Informação", "Engenharia"],
    image_url: "https://images.unsplash.com/photo-1528082992860-d520150d6f6c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=83ae28f06bf86dd0f6c9062858d2776a&auto=format&fit=crop&w=334&q=80"
  },
  {
    name: "Saúde",
    courses: ["Gastronomia", "Medicina"],
    image_url: "https://images.unsplash.com/photo-1519781542704-957ff19eff00?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=c351475860e8248dcd543ebbba1aa24f&auto=format&fit=crop&w=846&q=80"
  },
  {
    name: "Ciências Sociais Aplicadas",
    courses: ["Ciências Contábeis"],
    image_url: "https://images.unsplash.com/photo-1524995997946-a1c2e315a42f?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=a53f6ccb2fccd80ae7f6c6af8f289319&auto=format&fit=crop&w=750&q=80"
  },
  {
    name: "Ciências Humanas",
    courses: ["Direito"],
    image_url: "https://images.unsplash.com/photo-1527153907022-465ee4752fdc?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=ed05c707f6cb2dd027a718a8797d02f6&auto=format&fit=crop&w=700&q=80"
  }
].each_with_index do |q|
  field = StudyField.create(name: q[:name], image_url: q[:image_url])
  q[:courses].each { |c| field.courses.create(name: c) }
end

[
  # 5 respostas
  { text: "Ambiente cooperativo", category: 1 }, #1
  { text: "Ambientes extensos", category: 1 }, #2
  { text: "Trabalhar em laboratórios", category: 1 }, #3
  { text: "Trabalhar em restaurantes/bares/lanchonetes", category: 1 }, #4
  { text: "Ambiente externo (ar livre)", category: 1 }, #5
  { text: "Trabalhar em hospitais/clínicas", category: 1 }, #6
  { text: "Trabalhar em consultórios", category: 1 }, #7
  { text: "Trabalhar em hotéis", category: 1 }, #8
  { text: "Trabalhar em indústrias", category: 1 }, #9
  { text: "Trabalhar em escritórios", category: 1 }, #10
  { text: "Ambiente agitado", category: 1 }, #11
  { text: "Trabalhar em casa", category: 1 }, #12

  # 3 respostas
  { text: "Participar do avanço tecnológico", category: 2 }, #13
  { text: "Atividades rotineiras", category: 2 }, #14
  { text: "Contatos: relações de ajuda", category: 2 }, #15
  { text: "Comandar/liderar", category: 2 }, #16
  { text: "Construir", category: 2 }, #17
  { text: "Planejar", category: 2 }, #18
  { text: "Ler", category: 2 }, #19
  { text: "Desenhar", category: 2 }, #20
  { text: "Criar", category: 2 }, #21
  { text: "Desenvolver modelos matemáticos", category: 2 }, #22

  # 5 respostas
  { text: "Biologia", category: 3 }, #23
  { text: "Computação", category: 3 }, #24
  { text: "Instrumentos Cirúrgicos", category: 3 }, #25
  { text: "Culinária", category: 3 }, #26
  { text: "Legislação", category: 3 }, #27
  { text: "Estatística", category: 3 }, #28
  { text: "Construção", category: 3 }, #29
  { text: "Psicologia", category: 3 }, #30
  { text: "Desenhos", category: 3 }, #31
  { text: "Administração", category: 3 }, #32
  { text: "Números", category: 3 }, #33
  { text: "Fórmulas", category: 3 }, #34

  # 1 resposta
  { text: "Trabalhar em horário flexível", category: 4 }, #35
  { text: "Trabalhar em horário fixo", category: 4 }, #36
].each_with_index do |q, n|
  Question.create(q.merge(number: n+1))
end

[
  {
    field_name: "Ciências Exatas",
    questions: [2,5,9,12,13,14,16,17,18,20,21,22,24,28,29,31,33,34,35]
  },
  {
    field_name: "Saúde",
    questions: [1,3,4,6,7,8,15,19,23,24,25,26,34,35,36]
  },
  {
    field_name: "Ciências Sociais Aplicadas",
    questions: [10,14,16,28,32,33,35]
  },
  {
    field_name: "Ciências Humanas",
    questions: [7,10,11,12,14,15,19,27,30,35]
  }
].each do |r|
  field = StudyField.find_by_name(r[:field_name])
  r[:questions].each do |q|
    question = Question.find_by_number(q)
    FieldQuestion.create(study_field: field, question: question)
  end
end

questions_grouped = Question.all.to_a.group_by { |q| q.category }
questions_grouped.each do |category, questions|
  if questions.size > 6
    answers = Array.new(questions.size - 3, false)
    answers.concat(Array.new(3, true))
  else
    answers = Array.new((questions.size/2)+1, false)
    answers.concat(Array.new((questions.size/2), true))
  end
  answers.shuffle
  questions.each do |q|
    answer = answers.pop
    User.all.each { |u|
      u.answers.create!(question: q, content: answer)
      u.study_field = TestResultService.new(u).perform
      u.save
    }
  end
end
