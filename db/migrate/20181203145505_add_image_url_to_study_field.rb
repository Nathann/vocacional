class AddImageUrlToStudyField < ActiveRecord::Migration[5.1]
  def change
    add_column :study_fields, :image_url, :string
  end
end
