class AddFieldToUser < ActiveRecord::Migration[5.1]
  def change
    add_reference :users, :study_field, foreign_key: true
  end
end
