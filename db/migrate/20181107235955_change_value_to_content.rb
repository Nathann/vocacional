class ChangeValueToContent < ActiveRecord::Migration[5.1]
  def change
    rename_column :answers, :value, :content
  end
end
