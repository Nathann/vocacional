class CreateFieldQuestions < ActiveRecord::Migration[5.1]
  def change
    create_table :field_questions do |t|
      t.references :question, foreign_key: true
      t.references :study_field, foreign_key: true

      t.timestamps
    end
  end
end
