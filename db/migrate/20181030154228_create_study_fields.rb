class CreateStudyFields < ActiveRecord::Migration[5.1]
  def change
    create_table :study_fields do |t|
      t.string :name
      t.text :description

      t.timestamps
    end
  end
end
